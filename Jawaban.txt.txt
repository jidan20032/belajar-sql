1. Buat Database

=> 	create database myshop;


2. Buat table di dalam database

=>	a. users :
 		create table users (id int auto_increment primary key, name varchar (225), email varchar (225), password varchar(225));

	b. categories :
		create table categories (id int auto_increment primary key, name varchar (225));

	c. items :
		create table items (id int auto_increment primary key, name varchar (225), description varchar (225), price int, stock int, category_id int, foreign key(category_id) references categories(id));


3. Memasukkan data pada table

=>	a. users :

		INSERT INTO users (name, email)
    			-> values ("John Doe", "john@doe.com");
		INSERT INTO users (name, email)
    			-> values ("Jane Doe", "jane@doe.com");

	b. categories :

		INSERT INTO categories (name)
    			-> values ("gadget");
		INSERT INTO categories (name)
    			-> values ("cloth");
		INSERT INTO categories (name)
    			-> values ("men");
		INSERT INTO categories (name)
    			-> values ("women");
		INSERT INTO categories (name)
    			-> values ("branded");

	c. items :
		
		INSERT INTO items (name, description, price, stock, category_id)
    			-> values ("Sumsang b50", "hape keren dari merek sumsang", "1500000", "20", 1);
		INSERT INTO items (name, description, price, stock, category_id)
    			-> values ("Uniklooh", "baju keren dari brand ternama", "800000", "25", 2);
		INSERT INTO items (name, description, price, stock, category_id)
    			-> values ("IMHO Watch", "jam tangan anak yang jujur banget", "1000000", "30", 3);


4. Mengambil data dari database 

=>	a. Mengambil data users : 

		select id, name, email from users;

	b. Mengambil data items :

		- 	select*from items where price > 1000000;

		-	select*from items where name like "%sang%";

	c. Mengambil data items join dengan kategori :
	
		 select items.name, items.description, categories.name from items join categories on categories.id=items.category_id;


5. Mengubah data dari database :

=>	update items set price = "2500000" where id = 1;

		
	
	